FROM registry.gitlab.com/intellekt89/public/docker/dotnet-aspnet:8.0.11-bookworm-slim-amd64
# https://hub.docker.com/_/microsoft-dotnet-aspnet/

COPY release /app/bin
ADD --chown=1000:1000 https://gitlab.com/intellekt89/public/docker/ipropusk-gd-dockerfile/-/raw/main/entrypoint.sh /entrypoint.sh
COPY --chown=1000:1000 Ipropusk.gd.pfx /tmp/Ipropusk.gd.pfx

RUN chmod +x /entrypoint.sh && \
    chmod 600 /tmp/Ipropusk.gd.pfx && \
    ln -fs /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata && \
    chown -R 1000:1000 /app && \
    cat /etc/os-release && \
    dotnet --info

USER 1000

WORKDIR /app/bin
ENTRYPOINT ["/entrypoint.sh"]
CMD ["dotnet", "Ipropusk.gd.dll"]
